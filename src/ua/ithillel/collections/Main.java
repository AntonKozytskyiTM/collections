package ua.ithillel.collections;

import java.util.Arrays;

import ua.ithillel.collections.arraylist.IntList;
import ua.ithillel.collections.LinkedList.IntLinkedList;
public class Main {

  public static void main(String[] args) {
    IntList list = new IntLinkedList();

    list.add(10);
    list.add(15);
    list.add(17);
    list.add(15);
    list.add(3);

    // output: "[10,15,17,15,3]"
    printOut(list);

    // =============================================================================================
    list.add(3, 23);
    // output: "[10,15,17,23,15,3]"
    printOut(list);

    // =============================================================================================
    int element = list.get(2);
    // output: 17
    printOut(element);

    // =============================================================================================
    element = list.remove(2);

    // output: 17
    printOut(element);
    // output: "[10,15,23,15,3]"
    printOut(list);

    // =============================================================================================
    boolean result = list.removeByValue(15);

    // output: true
    printOut(result);
    // output: "[10,23,15,3]"
    printOut(list);

    // =============================================================================================
    int previousValue = list.set(1, 27);
    // output: 23
    printOut(previousValue);
    // output: "[10,27,15,3]"
    printOut(list);

    // =============================================================================================
    int size = list.size();
    // output: 4
    printOut(size);
    // =============================================================================================
    list.add(32);
    list.add(34);
    // output: "[10,27,15,3,32,34]"
    printOut(list);

    IntList subList = list.subList(2, 4);
    // output: "[15,3]"
    printOut(subList);

    // =============================================================================================
    boolean isEmpty = list.isEmpty();
    // output: false
    printOut(isEmpty);

    list.clear();

    isEmpty = list.isEmpty();
    // output: true
    printOut(isEmpty);

    // =============================================================================================
    list.add(8);
    list.add(13);

    int[] elements = list.toArray();

    // output: 8 13
    for (int el : elements) {
      System.out.printf("%s ", el);
    }
  }

  private static void printOut(IntList list) {
    System.out.println(Arrays.toString(list.toArray()));
  }

  private static void printOut(int element) {
    System.out.println(element);
  }

  private static void printOut(boolean result) {
    System.out.println(result);
  }
}
