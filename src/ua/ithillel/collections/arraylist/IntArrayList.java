package ua.ithillel.collections.arraylist;

public class IntArrayList implements IntList {

  @Override
  public void add(int element) {

  }

  @Override
  public void add(int index, int element) {

  }

  @Override
  public void clear() {

  }

  @Override
  public int get(int index) {
    return 0;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public int remove(int index) {
    return 0;
  }

  @Override
  public boolean removeByValue(int value) {
    return false;
  }

  @Override
  public int set(int index, int element) {
    return 0;
  }

  @Override
  public int size() {
    return 0;
  }

  @Override
  public IntList subList(int fromIndex, int toIndex) {
    return null;
  }

  @Override
  public int[] toArray() {
    return new int[0];
  }
}
